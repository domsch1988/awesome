#!/bin/sh

run() {
  if ! pgrep -f "$1" ;
  then
    "$@"&
  fi
}

run "autorandr" --change
run "nm-applet"
run "/home/dosa/.config/autostart-scripts/ssh-add.sh"
